package coupondunia.nearby.model;

public class Nearby {

    private String outletName;
    private String noCoupons;
    private String coverURL;
    private String logoURL;
    double latitude,longitude;
    private String category;
    private String neighbour;

    public String getNeighbour() {
        return neighbour;
    }

    public void setNeighbour(String neighbour) {
        this.neighbour = neighbour;
    }

    private float distance;

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getNoCoupons() {
        return noCoupons;
    }

    public void setNoCoupons(String noCoupons) {
        this.noCoupons = noCoupons;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public Nearby(String outletName,String noCoupons,String coverURL,String logoURL,String category,String neighbour,float distance,double latitude,double longitude) {
        super();
        this.outletName=outletName;
        this.noCoupons=noCoupons;
        this.coverURL=coverURL;
        this.logoURL=logoURL;
        this.latitude=latitude;
        this.longitude=longitude;
        this.category=category;
        this.distance=distance;
        this.neighbour=neighbour;
    }
}
