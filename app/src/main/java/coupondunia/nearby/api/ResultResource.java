package coupondunia.nearby.api;

import com.google.gson.JsonElement;
import retrofit.Callback;
import retrofit.http.GET;

public interface ResultResource {

    @GET("/task.txt")
    void getResults(Callback<JsonElement> response);
}
