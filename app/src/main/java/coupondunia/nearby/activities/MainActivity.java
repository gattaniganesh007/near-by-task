package coupondunia.nearby.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import coupondunia.nearby.HttpClient;
import coupondunia.nearby.R;
import coupondunia.nearby.adapter.NearByAdapter;
import coupondunia.nearby.api.ResultResource;
import coupondunia.nearby.model.Nearby;
import coupondunia.nearby.services.AppLocationService;
import coupondunia.nearby.utils.Connectivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    double clat,clng;
    String userAddress;

    @InjectView(R.id.search_textview) AutoCompleteTextView searchText;
    @InjectView(R.id.progress_bar) ProgressBar mMaterialProgressBar;
    @InjectView(R.id.no_internet_connection) TextView mInternetConnection;
    @InjectView(R.id.txt_retry) TextView mRetry;
    @InjectView(R.id.my_recycler_view) RecyclerView mRecyclerView;
    @InjectView(R.id.toolbar) Toolbar mToolbar;

    NearByAdapter nearByAdapter;
    ArrayList<Nearby> nearbyArrayList = new ArrayList<Nearby>();

    AppLocationService appLocationService;
    private Timer timer = new Timer();
    final long DELAY = 750; // in ms

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);
        initialiseToolbar(""+getResources().getString(R.string.title_name));
        hideProgressDialog();
        hideSoftKeyboard();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        nearByAdapter = new NearByAdapter(context, nearbyArrayList);
        mRecyclerView.setAdapter(nearByAdapter);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context,R.layout.list_item_nearby_suggest,R.id.text1,new String[]{});
        searchText.setAdapter(adapter);
        searchText.setThreshold(2);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                timer.cancel();
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.length() > 1) {
                    if (Connectivity.isConnected(context)) {
                        timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                new FetchSuggestionsResource(adapter).execute(s.toString());
                            }
                        }, DELAY);
                    }
                }
            }
        });

        searchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyboard();
                userAddress = adapter.getItem(position);
                searchText.setText("" + userAddress);
                new FetchLatLngTask(context).execute();
            }
        });

        appLocationService = new AppLocationService(context);
        Location location = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            clat = location.getLatitude();
            clng = location.getLongitude();
            fetchResult();
        } else {
            displayPromptForEnablingGPS();
        }
    }

    @OnClick(R.id.txt_retry)
    public void onClickRetry() {
        fetchResult();
        mRetry.setVisibility(TextView.GONE);
    }

    public void displayPromptForEnablingGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = ""+getResources().getString(R.string.enable_gps_msg);

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                mRetry.setVisibility(TextView.VISIBLE);
                                startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Manual",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    // Initialise toolbar title
    public void initialiseToolbar(String toolbarTitle){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(toolbarTitle);
    }

    public void fetchResult(){
        // Call to api through retrofit
        mMaterialProgressBar.setVisibility(View.VISIBLE);
        ResultResource questionAnswerResource = HttpClient.getInstance(getApplicationContext()).getClient(ResultResource.class, false);
        questionAnswerResource.getResults(new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {

                // Response from api
                String hash = jsonElement.getAsJsonObject().get("hash").getAsString();
                JsonObject mStatus = jsonElement.getAsJsonObject().getAsJsonObject("status");
                String statusMsg = mStatus.get("message").getAsString();
                int statusCode = mStatus.get("rcode").getAsInt();

                if (statusCode == 200) {
                    JsonArray mData = jsonElement.getAsJsonObject().getAsJsonArray("data");
                    int length = mData.size();

                    for (int i = 0; i < length; i++) {
                        JsonObject dataItem = mData.get(i).getAsJsonObject();
                        String outlet_name = dataItem.get("OutletName").getAsString();
                        String no_Coupon = dataItem.get("NumCoupons").getAsString();
                        String cover_URL = dataItem.get("CoverURL").getAsString();
                        String logo_URL = dataItem.get("LogoURL").getAsString();
                        String neightbour = dataItem.get("Distance").getAsInt() + " " + dataItem.get("NeighbourhoodName").getAsString();
                        double dlat = Double.parseDouble(dataItem.get("Latitude").getAsString());
                        double dlng = Double.parseDouble(dataItem.get("Longitude").getAsString());
                        JsonArray mCategory = dataItem.getAsJsonArray("Categories");
                        float actualdistance = distanceCalculate(clat, clng, dlat, dlng);
                        String category = mCategory.get(0).getAsJsonObject().get("Name").getAsString() + " , " + mCategory.get(1).getAsJsonObject().get("Name").getAsString();

                        // Add items into array list
                        nearbyArrayList.add(new Nearby(outlet_name, no_Coupon, cover_URL, logo_URL, category,neightbour ,actualdistance, dlat, dlng));
                    }
                } else {
                    mInternetConnection.setVisibility(TextView.VISIBLE);
                    mInternetConnection.setText("Server not responding");
                }

                // sort the arraylist by using distance
                Collections.sort(nearbyArrayList, new Comparator<Nearby>() {
                    @Override
                    public int compare(Nearby o1, Nearby o2) {
                        return (int)(o2.getDistance() - o1.getDistance());
                    }
                });

                nearByAdapter = new NearByAdapter(context, nearbyArrayList);
                mRecyclerView.setAdapter(nearByAdapter);
                hideProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                // TODO what to do if something goes wrong?
                hideProgressDialog();
                mInternetConnection.setVisibility(TextView.VISIBLE);
                mInternetConnection.setText("Something wen't wrong");
            }
        });
    }

    // hide the progressDialog
    public void hideProgressDialog() {
        mMaterialProgressBar.setVisibility(View.INVISIBLE);
    }

    // Calculate distance between current position to destination position
    public static float distanceCalculate(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    // fetch user suggestion address
    public class FetchSuggestionsResource extends AsyncTask<String, Void, ArrayList<String>>
    {
        //private static final String LOG_TAG = FetchTask.class.getSimpleName();
        private final ArrayAdapter<String> mArrayAdapter;

        public FetchSuggestionsResource(ArrayAdapter<String> mArrayAdapter) {
            this.mArrayAdapter = mArrayAdapter;
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String suggestionsJsonString = null;

            try {

                final String BASE_URL1 = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";

                Uri builtUri = Uri.parse(BASE_URL1).buildUpon()
                        .appendQueryParameter("key", "AIzaSyBNBQbQbhoAzr1P9H2Gbe8ZOtqZeaxT2Fc")
                        .appendQueryParameter("input", params[0])
                        .build();

                URL url = new URL(builtUri.toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                if (inputStream == null) {
                    return null;
                }

                StringBuilder buffer = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                suggestionsJsonString = buffer.toString();

                ArrayList<String> suggestions = new ArrayList<>();

                JSONObject suggestionsList = new JSONObject(suggestionsJsonString);
                if (suggestionsList.getString("status").equals("OK"))
                {
                    JSONArray predictions = suggestionsList.getJSONArray("predictions");
                    int length = predictions.length();

                    for (int i = 0; i < length; i++)
                    {
                        JSONObject prediction = predictions.getJSONObject(i);
                        suggestions.add(prediction.getString("description"));
                    }
                }

                return suggestions;
            } catch (IOException e) {
                //Log.e(LOG_TAG, "Error ", e);
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        //Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            super.onPostExecute(strings);
            mArrayAdapter.clear();
            mArrayAdapter.addAll(strings);
            mArrayAdapter.notifyDataSetChanged();
        }
    }


    // Fetch Latitude and Longitude of current address
    public class FetchLatLngTask extends AsyncTask<String, Void, String>
    {
        private Context mContext;

        public FetchLatLngTask(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String LatLngJsonString = null;

            try {

                final String BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?";

                Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                        .appendQueryParameter("key", "AIzaSyBNBQbQbhoAzr1P9H2Gbe8ZOtqZeaxT2Fc")
                        .appendQueryParameter("address", ""+userAddress)
                        .build();

                URL url = new URL(builtUri.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                if (inputStream == null) {
                    return null;
                }

                StringBuilder buffer = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }
                LatLngJsonString = buffer.toString();

                //Parse JSON string to get latitude, longitude of given address
                JSONObject LatLngJsonObj = new JSONObject(LatLngJsonString);
                if (LatLngJsonObj.getString("status").equals("OK"))
                {
                    JSONArray results = LatLngJsonObj.getJSONArray("results");
                    JSONObject addressDetail = results.getJSONObject(0);
                    JSONObject geometry = addressDetail.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");
                    String lat = location.getString("lat");
                    String lng = location.getString("lng");

                    return lat + "," + lng;
                }
            } catch (IOException e) {
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String[] parts = s.split(",");
            clat = Double.parseDouble(parts[0]);
            clng = Double.parseDouble(parts[1]);
            nearbyArrayList.clear();
            mRecyclerView.clearAnimation();
            fetchResult();
        }
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
