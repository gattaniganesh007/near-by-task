package coupondunia.nearby.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import butterknife.ButterKnife;
import butterknife.InjectView;
import coupondunia.nearby.R;
import coupondunia.nearby.model.Nearby;

public class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Nearby> mResultList;

    public NearByAdapter(Context context, ArrayList<Nearby> resultList) {
        mContext = context;
        mResultList = resultList;
    }

    @Override
    public NearByAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        View itemLayoutView =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_nearby, viewGroup, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(NearByAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.mDistance.setText(""+mResultList.get(i).getDistance());
        viewHolder.mOutletName.setText(mResultList.get(i).getOutletName());
        viewHolder.mCategoryName.setText(mResultList.get(i).getCategory());
        viewHolder.mOffer.setText(mResultList.get(i).getNoCoupons()+" offer");

        // Load the cover pic of outlet
        Picasso.with(mContext)
                .load(mResultList.get(i).getCoverURL())
                .fit().centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.mCoverImage);

        // Load the logo pic of outlet
        Picasso.with(mContext)
                .load(mResultList.get(i).getLogoURL())
                .fit().centerInside()
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.mLogoImage);

        //click to share outlet with other friends
        viewHolder.mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareHealthTip(i, mResultList.get(i).getOutletName(), mResultList.get(i).getNoCoupons());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mResultList.size();
    }

    public void shareHealthTip(int position, String outletName, String coupons){

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody;
        shareBody = outletName + "\n" + coupons + "\n";
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.cover_image) ImageView mCoverImage;
        @InjectView(R.id.logourl) ImageView mLogoImage;
        @InjectView(R.id.share) ImageView mShare;
        @InjectView(R.id.outlet_name) TextView mOutletName;
        @InjectView(R.id.category_name) TextView mCategoryName;
        @InjectView(R.id.distance) TextView mDistance;
        @InjectView(R.id.offer) TextView mOffer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}